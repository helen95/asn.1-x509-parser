package com.company;


import java.nio.ByteBuffer;
import java.util.Vector;
import java.math.BigInteger;

public class Parser {

    private ByteBuffer buf;
    private Vector<Block> vect;

    public Parser(ByteBuffer buf) {

        this.buf = buf;
        this.vect = new Vector<Block>();
    }

    private void getSubVec(final int start, final Block block) {

        if(block.len > 0) {

            int end = block.len + start;
            while (buf.position() < end) {

                Block tmp = decode_data();
                block.subBlockVec.add(tmp);
            }
        }
    }

    public Block decode_data() {

        Block block = new Block(this.buf);

        int start = block.getDataPos();

        if(block.tag.complex_block) {
            getSubVec(start, block);
        } else
            vect.add(block);

        if (block.subBlockVec.isEmpty()) {

            buf.position(start + block.len);
        }

        return block;
    }

    public String get_components() {

        String res = "";

        /*Version*/
        Block curr_block = vect.elementAt(0);
        if(curr_block.tag.tag_id == 0x02) {

            String s = Parse_components.parseInt(this.buf, curr_block);
            int vNum = 0;
            if(s.length() < 2) {
                vNum = Integer.valueOf(s);
            }
            if(vNum == 0) {

                res += "Version: v1\n";
            } else if(vNum == 1) {

                res += "Version: v2\n";
            } else {
                res += "Version: v3\n";
            }
        }

        /*Serial Number*/

        res += "Serial Number: ";

        curr_block = vect.elementAt(1);
        if(curr_block.tag.tag_id == 0x02)
            res += Parse_components.parseInt(buf, curr_block) + "\n";

        /*Other components*/

        for(int i = 2; i < vect.size(); i++) {
            switch (vect.elementAt(i).tag.tag_id) {
                case 0x06: //OBJECT IDENTIFIER
                    res += parseVec(vect.elementAt(i)) + ": ";
                    break;
                case 0x03: //BIT STRING
                    if(i == vect.size() - 1) {
                        res += "\n" + parseVec(vect.elementAt(i));
                        break;
                    }
                case 0x04: //OCTET STRING
                case 0x13: //PrintableString
                case 0x16: //IA5String
                case 0x17: //UTCTime
                    res += parseVec(vect.elementAt(i)) + "\n";
                    break;
            }
        }
        return res;
    }


    private String parseVec(final Block block) {

        switch (block.tag.tag_id) {
            case 0x01: //BOOLEAN
                return (buf.get(block.getDataPos()) == 0x00) ? "False" : "True";
            case 0x02: //INTEGER
                return Parse_components.parseInt(buf, block);
            case 0x03: //BIT STRING
                return Parse_components.parseBitStr(buf, block);
            case 0x04: //OCTET STRING
                return Parse_components.parseOctets(buf, block);
            case 0x05: //NULL
                return "NULL";
            case 0x06: //OBJECT IDENTIFIER
                return Parse_components.parseOID(buf, block);
            case 0x13: //PrintableString
            case 0x16: //IA5String
                return Parse_components.parseString(buf, block);
            case 0x17: //UTCTime
                return Parse_components.parseTime(buf, block);
        }
        return "";
    }
}
