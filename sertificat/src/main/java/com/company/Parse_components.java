package com.company;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parse_components {

    public static String parseInt(final ByteBuffer buf, final Block block) {

        int start = block.start_pos + block.header;
        int end = start + block.len;
        int l = block.len;
        String res = "";
        int b = (int) buf.get(start);
        boolean sign = (b <= 127) ? false : true;
        int mask_sign = sign ? 255 : 0;

        if(l > 4) { // > int
            l <<= 3;
            int tmp = b;
            while(((tmp ^ mask_sign) & 0x80) == 0) {
                tmp <<= 1;
                l--;
            }
            res += "[" + l + "bit]\n";
        }

        if(sign)
            b += 256;

        BigInteger a = BigInteger.valueOf(b);
        for(int i = start + 1; i < end; i++) {
            a = a.multiply(BigInteger.valueOf(256));
            int tmp = buf.get(i);
            if(tmp < 0) {
                tmp += 256;
            }
            a = a.add(BigInteger.valueOf(tmp));
        }

        return res + a.toString(16);
    }

    public static String parseOID(final ByteBuffer buf, Block block) {

        int start = block.start_pos + block.header;
        int end = start + block.len;
        int count_bits = 0;
        String res = "";
        BigInteger a = BigInteger.valueOf(0);
        Integer SID1;
        BigInteger SID2;

        for(int i = start; i < end; i++) {
            a = a.multiply(BigInteger.valueOf(128));
            int tmp = (int)buf.get(i);
            if(tmp < 0) {
                tmp += 256;
            }
            a = a.add(BigInteger.valueOf(tmp & 0x7F));
            count_bits += 7;
            if((tmp & 0x80) == 0) {
                if(res.contentEquals("")) {
                    SID1 = 2;
                    if(a.compareTo(BigInteger.valueOf(80)) < 0) {

                        if(a.compareTo(BigInteger.valueOf(40)) < 0) {
                            SID1 = 0;
                        } else {
                            SID1 = 1;
                        }
                    }
                    SID2 = a.subtract(BigInteger.valueOf(SID1).multiply(BigInteger.valueOf(40)));
                    res += SID1.toString() + "." + SID2.toString();
                } else {
                    res += "." + a.toString();
                }

                a = BigInteger.valueOf(0);
                count_bits = 0;
            }
        }

        if(count_bits > 0)
            res += ".incomplete";
        return res;
    }

    public static String parseString(final ByteBuffer buf, final Block block) {

        int start = block.start_pos + block.header;
        int end = start + block.len;
        String res = "";

        for(int i = start; i < end; i++) {

            int b = (int)buf.get(i);
            if(b < 0) {
                b += 256;
            }
            res += String.valueOf((char)b);
        }
        return res;
    }

    public static String parseTime(final ByteBuffer buf, final Block block) {

        String s = Parse_components.parseString(buf, block);

        final String Time_pt =    // YYMMDDHHMMSS Z|[+-]hhmm
                "^(\\d\\d)" +         // YY
                "(0[1-9]|1[0-2])" +   // MM
                "(0[1-9]|[12]\\d|3[01])" + // DD
                "([01]\\d|2[0-3])" +       // HH
                "(?:([0-5]\\d)(?:([0-5]\\d)(?:[.,](\\d{1,3}))?)?)?(Z|[-+](?:[0]\\d|1[0-2])([0-5]\\d)?)?"; // MMSS Z|[+-]hhmm

        Pattern pt = Pattern.compile(Time_pt);
        Matcher mch = pt.matcher(s);

        if(mch.matches() == false) {
            return "Unrecognize TimeString" + s;
        }

        Integer y = Integer.valueOf(mch.group(1));
        y += y < 70 ? 2000 : 1900;

        String res = y.toString() + "/" + mch.group(2) + "/" + mch.group(3) + " " + mch.group(4);

        if(mch.group(5) != null) {

            res += ":" + mch.group(5) + ":" + mch.group(6);
            if(mch.group(7) != null) {
                res += "." + mch.group(7);
            }
        }

        if(mch.group(8) != null) {

            res += " GMT ";
            if(mch.group(8).equals("Z") == false) {
                res += mch.group(8) + ":" + mch.group(9);
            }
        }
        return res;
    }

    public static String parseBitStr(final ByteBuffer buf, final Block block) {

        int start = block.start_pos + block.header;
        int end = start + block.len;
        String res = "";
        int unused = buf.get(start);

        for(int i = start; i < end; i++) {
            int b = (int) buf.get(i);
            if(b < 0) {
                b += 256;
            }
            if(i == end - 1) {
                b >>= unused;
            }
            res += String.format("%02x", b);
        }

        return res;
    }

    private static boolean isASCII(final ByteBuffer buf, final Block block) {

        int start = block.start_pos + block.header;
        int end = start + block.len;

        for(int i = start; i < end; i++) {

            int b = (int)buf.get(i);
            if(b < 0)
                b += 256;
            if(b > 127 || b < 32)
                return false;
        }
        return true;
    }

    public static String parseOctets(final ByteBuffer buf, final Block block) {

        if (isASCII(buf, block)) {
            return Parse_components.parseString(buf, block);
        }
        int start = block.start_pos + block.header;
        int end = start + block.len;
        String res = "";
        int l = block.len;

        for(int i = start; i < end; i++) {

            int b = (int)buf.get(i);
            if(b < 0)
                b += 256;
            res += String.format("%02x", b);
        }

        return res;
    }
}
