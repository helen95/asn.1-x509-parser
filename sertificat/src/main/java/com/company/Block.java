package com.company;

import java.nio.ByteBuffer;
import java.util.Vector;


public class Block {

    Tag tag;
    int start_pos, len, header;
    Vector<Block> subBlockVec;

    Block(ByteBuffer buf) {
        tag = new Tag(buf);
        start_pos = buf.position();
        len = get_len(buf);
        header = buf.position() - start_pos;
        subBlockVec = new Vector<Block>();
    }

    public int getDataPos() {
        return start_pos + header;
    }

    private int get_len(ByteBuffer buf) {

        int b = (int)buf.get();
        if(((b >> 7) & 0x01) == 1) {

            int count_lBlocks = b & 0x7F;

            b = 0;
            for(int i = 0; i < count_lBlocks; i++) {
                int tmp = (int)buf.get();
                if(tmp < 0)
                    tmp += 256;
                b = (b << 8) + tmp;
            }
        }
        return b;
    }

    class Tag {
        int block_class, tag_id;
        boolean complex_block;

        Tag(ByteBuffer buf) {
            int b = (int)buf.get();
            if(b < 0)
                b += 256;
            block_class = b >> 6;
            tag_id = b & 0x1F;
            complex_block = ((b & 0x20) >> 5 == 1);
        }

        boolean is_0() {
            return (block_class == 0x00);
        }
    }
}
