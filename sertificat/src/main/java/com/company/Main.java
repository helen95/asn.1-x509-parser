package com.company;


import java.io.*;
import java.nio.ByteBuffer;


public class Main {
    private static ByteBuffer buf;

    public static void main(String[] args) {

        InputStream reader = null;

        String path = "src" + File.separator + "main" + File.separator + "java" + File.separator + "com" + File.separator + "company" + File.separator + "certificate2.cer";

        try {

            FileInputStream f = new FileInputStream(path);

            byte[] buffer = new byte[f.available()];
            f.read(buffer, 0, f.available());

            buf = ByteBuffer.wrap(buffer);
            Parser p = new Parser(buf);
            p.decode_data();

            String res = p.get_components();
            System.out.println(res);
        }
        catch (IOException e){

            System.out.println("File is not found");
        }

    }

}

